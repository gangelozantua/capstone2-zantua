const express = require('express');
const router = express.Router();
const orderControllers = require('../controllers/orderControllers')

const auth = require("../auth");
const {verify, verifyAdmin, verifySuperAdmin} = auth;


router.post('/', verify, orderControllers.createOrder);

router.get('/getAllOrders', verify, verifyAdmin, orderControllers.getAllOrders)

router.get('/getUserOrders', verify, orderControllers.getUserOrders)


module.exports = router;