const express = require('express');
const app = express();
const mongoose = require('mongoose');

const cors= require("cors");



const port = process.env.PORT || 4000;


mongoose.connect('mongodb+srv://angzang:angelo@cluster0.j7r8u.mongodb.net/capstone2?retryWrites=true&w=majority',
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	});


let db = mongoose.connection;
db.on('error',console.error.bind(console, "Connection Error"));
db.once('open',() => console.log("Connected to MongoDB"));

const corsOptions = {

	origin: ["http://localhost:3000", "https://musing-montalcini-91785f.netlify.app"],
	optionsSuccessStatus : 200
}

app.use(cors(corsOptions))
app.use(express.json());

const userRoutes = require('./routes/userRoutes')
const productRoutes = require('./routes/productRoutes')
const orderRoutes = require('./routes/orderRoutes')

app.use('/users', userRoutes);
app.use('/products', productRoutes);
app.use('/orders', orderRoutes);


app.listen(port, () => console.log(`Server is running at port ${port}`));