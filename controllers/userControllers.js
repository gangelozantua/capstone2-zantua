const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth');

module.exports.registerUser = (req,res) => {

	//console.log(req.body)
	let validatePassword = req.body.password

	if(validatePassword.length < 8){
		return res.send("Password must be 8 or more characters long.")
	}

	const hashedPW = bcrypt.hashSync(req.body.password,10)
	//console.log(hashedPW)

	User.findOne({email: req.body.email})
	.then(user => {

		if(user !== null){
			return res.send({message:"Email already exists."})
			
		}
		else{
			let newUser = new User({

				firstName : req.body.firstName,
				lastName : req.body.lastName,
				email : req.body.email,
				password : hashedPW,
				mobileNo : req.body.mobileNo
			})

			newUser.save()			
			return res.send(newUser)
		}
	})
	.catch(error => res.send(error))
}


module.exports.login = (req,res) => {

	//console.log(req.body)

	User.findOne({email: req.body.email})
	.then(result => {

		if(result === null){
			return res.send({message:"No user Found."})
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(req.body.password,result.password)
			
			if(isPasswordCorrect){
				return res.send({accessToken: auth.createAccessToken(result)})
			}
			else{
				return res.send({message:"Incorrect Password."})
			}
		}
	})
	.catch(error => res.send(error))
}


module.exports.updateAdmin = (req,res) => {

	User.findByIdAndUpdate(req.params.id, {isAdmin: true}, {new: true})
	.then(user => res.send(user))
	.catch(error => res.send(error))
}



module.exports.getUserDetails = (req,res) => {
	
	User.findById(req.user.id)
	.then(user => res.send(user))
	.catch(error => res.send(error))
}





