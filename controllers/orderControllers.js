const Order = require('../models/Order');
const User = require('../models/User')
const Product = require('../models/Product')

module.exports.createOrder = async (req,res) => {

	//console.log(req.user)
	//console.log(req.body.products)

	let products = req.body.products

	let totalAmount = 0

	for(i = 0; i < products.length; i++){

		let productId = products[i].productId
		let quantityAmount = products[i].quantity

		await Product.findById(productId).then(product => {
			
			return totalAmount += product.price * quantityAmount

		}).catch(error => res.send(error))
	}
	
	console.log(totalAmount)

	let newOrder = new Order({

		userId: req.user.id,
		totalAmount: totalAmount,
		products: req.body.products

	})

	let isOrderSaved = await newOrder.save().then(result => true).catch(error => error.message)

	//console.log(newOrder)
	//console.log(newOrder._id)

	let newOrderId = newOrder._id


	let isProductsUpdated = await Order.findById(newOrderId).then(result => {

		let products = newOrder.products
	
		for(i = 0; i < products.length; i++){

			let productId = products[i].productId
			let quantityAmount = products[i].quantity

			Product.findById(productId).then(result => {

				//console.log(result)

				let orders = {
					orderId: newOrderId,
					quantity: quantityAmount
				}
				result.orders.push(orders)

				//console.log(result.orders)
				result.save().then(result => console.log(result)).catch(error => res.send(error))
								
			})			
		}
		return true
	})

	//console.log(isProductsUpdated)

	if(isProductsUpdated && isOrderSaved){
		return res.send({message:`Order Successful. Your total amount is P${totalAmount}.`})
	}

}


module.exports.getAllOrders = (req,res) => {

	Order.find({})
	.then(orders => res.send(orders))
	.catch(error => res.send(error))
}


module.exports.getUserOrders = (req,res) => {

	//console.log(req.user)

	Order.find({userId: req.user.id})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}