const Product = require('../models/Product');

module.exports.addProduct = (req,res) => {

	//console.log(req.body)

	Product.findOne({name: req.body.name})
	.then(product => {

		if(product !== null){
			return res.send({message: "Product already exists."})
			
		}
		else{
			let newProduct = new Product({

				name : req.body.name,
				description : req.body.description,
				price : req.body.price,
				imageUrl: req.body.imageUrl

			})

			newProduct.save()			
			return res.send(newProduct)
		}
	})
	.catch(error => res.send(error))

}


module.exports.getActiveProducts = (req,res) => {

	Product.find({isActive: true})
	.then(products => res.send(products))
	.catch(error => res.send(error))
}

module.exports.getAllProducts = (req,res) => {

	Product.find({})
	.then(products => res.send(products))
	.catch(error => res.send(error))
}


module.exports.getSingleProduct = (req,res) => {

	Product.findById(req.params.id)
	.then(product => res.send(product))
	.catch(error => res.send(error))
}


module.exports.updateProduct = (req,res) => {

	let updates = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
		imageUrl: req.body.imageUrl
	}

	Product.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(product => res.send(product))
	.catch(error => res.send(error))
}


module.exports.archiveProduct = (req,res) => {

	Product.findByIdAndUpdate(req.params.id, {isActive: false}, {new: true})
	.then(product => res.send(product))
	.catch(error => res.send(error))
}


module.exports.activateProduct = (req,res) => {

	Product.findByIdAndUpdate(req.params.id, {isActive: true}, {new: true})
	.then(product => res.send(product))
	.catch(error => res.send(error))
}


module.exports.findProductName = (req,res) => {

	//console.log(req.body)

	Product.find({name: {$regex: req.body.name, $options: '$i'}})
	.then(product => {

		if(product.length === 0){
			return res.send({message:"No product found."})
		}
		else{
			//console.log(result)
			return res.send(product)
		}
	})
	.catch(error => res.send(error))
}


module.exports.getNewProducts = (req,res) => {

	Product.find({isActive: true}).sort({ _id: -1 }).limit(3)
	.then(products => res.send(products))
	.catch(error => res.send(error))
}

module.exports.getHotProducts = (req,res) => {

	Product.find({isActive: true}).sort({ __v: -1 }).limit(3)
	.then(products => res.send(products))
	.catch(error => res.send(error))
}