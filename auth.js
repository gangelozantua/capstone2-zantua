const jwt = require("jsonwebtoken");
const secret = "Capstone2";
const User = require('./models/User');

module.exports.createAccessToken = (user) => {

	//console.log(user)
	const data = {

		id: user._id
	}

	return jwt.sign(data,secret,{});
}


module.exports.verify = (req,res,next) => {

	//console.log(req.headers.authorization)
	let token = req.headers.authorization;

	if(typeof token === "undefined"){
		return res.send("Authorization failed. No token.")
	}
	else {
		token = token.slice(7,token.length)
		//console.log(token)

		jwt.verify(token, secret, (error,decodedToken) => {

			if(error){
				return res.send(error)
			}
			else {
				req.user = decodedToken;

				next();
			}
		})
	}
}


module.exports.verifyAdmin = (req,res,next) => {

	//console.log(req.user);

	User.findById(req.user.id)
	.then(user => {
		if(user.isAdmin){
			next();
		}
		else{
			return res.send("Action forbidden.")
		}
	})
	.catch(error => res.send(error))
}
