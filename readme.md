Project Name: E commerce API

Admin Credentials:

email: "admin@gmail.com"
password: "admin123"


Regular User Credentials:

email: "sampleUser@gmail.com"
password: "sample123"

Features:

User registration
User authentication
Set User as admin(admin only)
Retriecve all active products
Retrieve single product
Create Product (admin only)
Update Product information (admin only)
Archive Product (admin only)
Non - admin User checkout (create Order)
Retrieve authenticated user's orders
Retrieve all orders (admin only)